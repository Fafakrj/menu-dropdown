<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/styles/style.css">
    <title>Menu</title>
</head>

<body>
    <h1>Menu dropdown fait avec laravel.</h1>
    <div id="container">
        <nav>
            <ul>
                <li><a href="#">Chouabi</a></li>
                <li><a href="#">Applications</a>

                    <ul>
                        <li><a href="#">ENNOV - Gestion documentaire</a></li>
                        <li><a href="#">Evénement indésirables - ENNOV</a></li>
                        <li><a href="#">Informatique</a></li>
                        <li><a href="#">Developpement</a>

                            <ul>
                                <li><a href="#">1. YOY - menu / groupe</a></li>
                                <li><a href="#">2. Statitics Intranet</a></li>
                                <li><a href="#">3. Monitoring</a>
                                <li><a href="#">4. Traduction Dev</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Cardiologie</a></li>
                        <li><a href="#">Reporting</a></li>
                        <li><a href="#">Annulation RDV</a></li>
                    </ul>
                </li>
                
                <li><a href="#">Info</a>
                    <ul>
                        <li><a href="#">Annuaire interne - Telefoongids</a></li>
                        <li><a href="#">Annuaire fonctionnel - Dienstengids</a></li>
                        <li><a href="#">Crèche - kinderkribbe</a></li>
                        <li><a href="#">Cybersécrité & RGPD</a></li>
                        <li><a href="#">Effet ou objet personnel trouvé</a></li>
                        <li><a href="#">Energie : URE - REG</a></li>
                    </ul>
                </li>
                <li><a href="#">RH - HR</a>
                    <ul>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </li>
                <li><a href="#">Ressources</a>
                    <ul>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</body>

</html>